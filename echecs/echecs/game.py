from ast import literal_eval
from termcolor import colored
from echecs.possible import case_occupee_par_noirs
from echecs.possible import case_occupee_par_blancs
from echecs.possible import mouvements_possibles
from echecs.initialisation import liste_pieces_noires_init
from echecs.initialisation import liste_pieces_blanches_init
from echecs.initialisation import creer_matrice_pieces
from echecs.possible import afficher_liste_mouvements_possibles
from echecs.possible import trouver_piece
from echecs.possible import est_en_echec
from copy import deepcopy

#def tourne_echiquier(grid):
#    # tourne l'échiquier à chaque tour :
#    nouvel_echiquier = 8 * [8 * [0]]
#    for ligne in range(8):
#        for colonne in range(8):
#            nouvel_echiquier[ligne][colonne] = grid[7-ligne][7-colonne]
#    return nouvel_echiquier


def supprime_piece(liste, position):     # supprime la piece de la liste qui se trouve dans la position indiquée
    return [piece for piece in liste if piece['position'] != position]


def mouvement(liste_pieces_noires, liste_pieces_blanches, xdebut, ydebut, xfin, yfin):
        piece = trouver_piece((xdebut, ydebut), liste_pieces_noires, liste_pieces_blanches)
        piece['position'] = (xfin, yfin)
        # on teste si une pièce a été mangée :
        couleur = piece['type']['couleur']
        if couleur == 'blanc':
            if (xfin, yfin) in case_occupee_par_noirs(liste_pieces_noires):
                piece_mangee = [piece for piece in liste_pieces_noires if piece['position'] == (xfin, yfin)][0]
                liste_pieces_noires.remove(piece_mangee)
        else:
            if (xfin, yfin) in case_occupee_par_blancs(liste_pieces_blanches):
                piece_mangee = [piece for piece in liste_pieces_blanches if piece['position'] == (xfin, yfin)][0]
                liste_pieces_blanches.remove(piece_mangee)
        return liste_pieces_blanches, liste_pieces_noires


def display_grid(matrice_pieces):
    # Prend en argument une matrice (liste de listes), et crée le plateau associé
        m = 2 * 8 + 1
        line = ' '
        for i in range(8):
            line = line + '==== '
        L = []
        for i in range(8):
            l = '|'
            for j in range(8):
                    l = l + ' ' + str(matrice_pieces[i][j]) + ' |'

            L.append(l + ' {}'.format(i))
        for i in range(m):
            if i % 2 == 0:
                print(line)
            else:
                print(L[i // 2])
        print('  0    1    2    3    4    5    6    7  ')


def read_player_command_blanc(grid, LN, LB):
    while True:
        try:
            coordonnees_piece_a_deplacer = literal_eval(input("\nJoueur 1, choisissez la pièce blanche à déplacer (numéro ligne,numéro colonne) :"))
            piece = trouver_piece(coordonnees_piece_a_deplacer, LN, LB)
            nombre_de_mouvements_possibles = len(mouvements_possibles(piece, grid))
            if piece['type']['couleur'] == 'blanc':
                if nombre_de_mouvements_possibles != 0:
                    return coordonnees_piece_a_deplacer
                else:
                    print("\nCette pièce ne peut pas se déplacer")
            else:
                print("\nVeuillez sélectionner une pièce blanche")
        except:
            print('\nAucune pièce à cet endroit')


def read_player_command_noir(grid, LN, LB):
    while True:
        try:
            coordonnees_piece_a_deplacer = literal_eval(input("\nJoueur 2, choisissez la pièce noire à déplacer (numéro ligne,numéro colonne) :"))
            piece = trouver_piece(coordonnees_piece_a_deplacer, LN, LB)
            nombre_de_mouvements_possibles = len(mouvements_possibles(piece, grid))
            if piece['type']['couleur'] == 'noir':
                if nombre_de_mouvements_possibles != 0:
                    return coordonnees_piece_a_deplacer
                else:
                    print("\nCette pièce ne peut pas se déplacer")
            else:
                print("\nVeuillez sélectionner une pièce noire")
        except:
            print('\nAucune pièce à cet endroit')


def coordonnees_piece_vers(liste_move_possible):
    while True:
        try:
            demande_coordonnees_piece_vers = literal_eval(input("\nChoisissez où déplacer la pièce sélectionnée(numéro ligne,numéro colonne) :"))
            if demande_coordonnees_piece_vers in liste_move_possible:
                return demande_coordonnees_piece_vers
            else:
                print("\nCe mouvement est impossible")
        except:
            print('\nChoix non valide')


def est_echec_et_mat2(LB, LN, couleur, grid):
    LB2, LN2 = deepcopy(LB), deepcopy(LN)
    if couleur == 'noir':
        for piece in LN:
            xdebut, ydebut = piece['position']
            for move in mouvements_possibles(piece, grid):
                xfin, yfin = move
                LB2, LN2 = mouvement(LN2, LB2, xdebut, ydebut, xfin, yfin)
                grid2 = creer_matrice_pieces(LN2, LB2)
                if not est_en_echec(LB2, LN2, 'noir', grid2):
                    return False
                LB2, LN2 = deepcopy(LB), deepcopy(LN)
        return True
    if couleur == 'blanc':
        for piece in LB:
            xdebut, ydebut = piece['position']
            for move in mouvements_possibles(piece, grid):
                xfin, yfin = move
                LB2, LN2 = mouvement(LN2, LB2, xdebut, ydebut, xfin, yfin)
                grid2 = creer_matrice_pieces(LN2, LB2)
                if not est_en_echec(LB2, LN2, 'blanc', grid2):
                    return False
                LB2, LN2 = deepcopy(LB), deepcopy(LN)
        return True


def game_chess():
    compteur = 0
    LN = liste_pieces_noires_init()
    LB = liste_pieces_blanches_init()
    M = creer_matrice_pieces(LN, LB)
    display_grid(M)
    print("\nJoueur 1, vous utilisez les pièces blanches\nJoueur 2, vous utilisez les pièces noires")
    while not est_echec_et_mat2(LB,LN,'noir',M) and not est_echec_et_mat2(LB, LN, 'blanc', M):
        if compteur % 2 == 0:
            if est_en_echec(LB, LN, 'blanc', M):
                print(colored("\nRoi blanc en échec", 'red'))
            coordonnees_piece_a_deplacer = read_player_command_blanc(M, LN, LB)
            xdebut, ydebut = coordonnees_piece_a_deplacer
            liste_move_possible = afficher_liste_mouvements_possibles(coordonnees_piece_a_deplacer, M, LN, LB)
            print("\nMouvements possibles : {}".format(liste_move_possible))
            coordonnees_piece = coordonnees_piece_vers(liste_move_possible)
            xfin, yfin = coordonnees_piece
            LB, LN = mouvement(LN, LB, xdebut, ydebut, xfin, yfin)
            M = creer_matrice_pieces(LN, LB)
            display_grid(M)
        else:
            if est_en_echec(LB, LN, 'noir', M):
                print(colored("\nRoi noir en échec", 'red'))
            coordonnees_piece_a_deplacer = read_player_command_noir(M, LN, LB)
            xdebut, ydebut = coordonnees_piece_a_deplacer
            liste_move_possible = afficher_liste_mouvements_possibles(coordonnees_piece_a_deplacer, M, LN, LB)
            print("Mouvements possibles : {}".format(liste_move_possible))
            coordonnees_piece = coordonnees_piece_vers(liste_move_possible)
            xfin, yfin = coordonnees_piece
            LB, LN = mouvement(LN, LB, xdebut, ydebut, xfin, yfin)
            M = creer_matrice_pieces(LN, LB)
            display_grid(M)
        compteur += 1

    if compteur % 2 == 0:
        print("\nEchec et mat du roi blanc\n\nLe joueur 2 remporte la partie !")
    else:
        print("\nEchec et mat du roi noir\n\nLe joueur 1 remporte la partie !")


if __name__ == '__main__':
    game_chess()


