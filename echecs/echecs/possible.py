from echecs.initialisation import pieces_blanches
from echecs.initialisation import pieces_noires
from echecs.initialisation import creer_matrice_pieces


"""PARTIE ECHEC"""


def trouver_piece(coordonnees, LN, LB):
    # prend en arguments les coordonnees choisies par le joueur, les pieces noires et blanches, et retourne la pièce qui est aux coordonnees choisies
    for piece in LN:
        if piece['position'] == coordonnees:
            return piece
    for piece in LB:
        if piece['position'] == coordonnees:
            return piece


def case_menacee_par_pion(piece):
    res = []
    ligne, colonne = piece['position']
    couleur = piece['type']['couleur']
    if couleur == 'blanc':
        if ligne >= 1:
            if colonne >= 1:
                res.append((ligne - 1, colonne - 1))
            if colonne <= 6:
                res.append((ligne - 1, colonne + 1))
    if couleur == 'noir':
        if ligne <= 6:
            if colonne >= 1:
                res.append((ligne + 1, colonne - 1))
            if colonne <= 6:
                res.append((ligne + 1, colonne + 1))
    return res


def cases_menacees(liste, grid):  # liste des cases menacées par les pièces contenues dans liste
    liste_cases_menacees = []
    for piece in liste:
        if piece['type']['type'] != 'pion':
            liste_cases_menacees = reunion(mouvements_possibles(piece, grid), liste_cases_menacees)
        else:
            liste_cases_menacees = reunion(liste_cases_menacees, case_menacee_par_pion(piece))
    return liste_cases_menacees


def est_en_echec(liste_des_blancs, liste_des_noirs, couleur, grid):    # teste si le roi de la couleur demandée est en échec
    if couleur == 'noir':
        localise_roi = [piece['position'] for piece in liste_des_noirs if piece['type']['type'] == 'roi']
        ligne, colonne = localise_roi[0]  # position du roi noir
        return (ligne, colonne) in cases_menacees(liste_des_blancs, grid)
    if couleur == 'blanc':
        localise_roi = [piece['position'] for piece in liste_des_blancs if piece['type']['type'] == 'roi']
        ligne, colonne = localise_roi[0]  # position du roi blanc
        return (ligne, colonne) in cases_menacees(liste_des_noirs, grid)


def est_bloque(LB, LN, couleur, grid):
    if couleur == 'noir':
        for piece in LN:
            xdebut, ydebut = piece['position']
            for move in mouvements_possibles(piece, grid):
                xfin, yfin = move
                if ce_mouvement_est_possible(LN, LB, xdebut, ydebut, xfin, yfin):
                    return False
        return True
    else:
        for piece in LB:
            xdebut, ydebut = piece['position']
            for move in mouvements_possibles(piece, grid):
                xfin, yfin = move
                if ce_mouvement_est_possible(LN, LB, xdebut, ydebut, xfin, yfin):
                    return False
        return True


def est_echec_et_mat(LB, LN, couleur, grid):
    return est_en_echec(LB, LN, couleur, grid) and est_bloque(LB, LN, couleur, grid)


def fin_de_partie(LB, LN):
    grid = creer_matrice_pieces(LN, LB)
    return est_echec_et_mat(LB, LN, 'blanc', grid) or est_echec_et_mat(LB, LN, 'noir', grid)


"""FIN PARTIE ECHEC"""


"""PARTIE POSSIBLE"""


def reunion(liste1, liste2):
    liste_finale = []
    for element in liste1:
        if element not in liste_finale:
            liste_finale.append(element)
    for element in liste2:
        if element not in liste_finale:
            liste_finale.append(element)
    return liste_finale


def position_valide(t):
    i, j = t
    return 0 <= i <= 7 and 0 <= j <= 7


def case_occupee_par_noirs(liste_noirs):
    return [piece['position'] for piece in liste_noirs]


def case_occupee_par_blancs(liste_blancs):
    return [piece['position'] for piece in liste_blancs]


def depl_roi_possible(grid, x, y):       # roi en position (x,y)
    roi = grid[x][y]
    couleur = roi[1]
    if couleur == 'N':
        liste = [(a, b) for a in range(8) for b in range(8) if
                 ((abs(a-x) == 1 and abs(b-y) <= 1) or (abs(a-x) <= 1 and abs(b-y) == 1)) and grid[a][b][-1] in [' ', 'B']]
        return liste
    if couleur == 'B':
        liste = [(a, b) for a in range(8) for b in range(8) if
                 ((abs(a-x) == 1 and abs(b-y) <= 1) or (abs(a-x) <= 1 and abs(b-y) == 1)) and grid[a][b][-1] in [' ', 'N']]
        return liste


def depl_cavalier_possible(grille, coordonnees):
    dep = []
    ligne = coordonnees[0]
    colonne = coordonnees[1]
    dep_de_base = [(ligne - 2, colonne - 1), (ligne - 1, colonne - 2), (ligne - 2, colonne + 1), (ligne - 1, colonne + 2), (ligne + 2, colonne - 1), (ligne + 1, colonne - 2), (ligne + 2, colonne + 1), (ligne + 1, colonne + 2)]
    for case in dep_de_base:
        if position_valide(case) and grille[case[0]][case[1]][-1] != grille[ligne][colonne][-1]:
            dep.append(case)
    return dep


def depl_pion_noir_possible(grid, position):  # arguments: état du plateau (matrice), position (tuple),renvoie liste de tuples des cases possibles
    ligne, colonne = position
    liste_depl = []
    if ligne == 1:
        if grid[3][colonne] == '  ' and grid[2][colonne] == '  ':
            liste_depl.append((3, colonne))
    if ligne + 1 <= 7 and grid[ligne + 1][colonne] == '  ':
        liste_depl.append((ligne+1, colonne))
    if ligne + 1 <= 7 and colonne-1 >= 0 and grid[ligne + 1][colonne - 1] in pieces_blanches:
        liste_depl.append((ligne+1, colonne-1))
    if ligne + 1 <= 7 and colonne + 1 <= 7 and grid[ligne + 1][colonne + 1] in pieces_blanches:
        liste_depl.append((ligne + 1, colonne + 1))
    return liste_depl


def depl_pion_blanc_possible(grid, position):  # arguments: état du plateau (matrice), position (tuple),renvoie liste de tuples des cases possibles
    ligne, colonne = position
    liste_depl = []
    if ligne == 6:
        if grid[4][colonne] == '  ' and grid[5][colonne] == '  ':
            liste_depl.append((4, colonne))
    if ligne - 1 >= 0 and grid[ligne - 1][colonne] == '  ':
        liste_depl.append((ligne - 1, colonne))
    if ligne - 1 >= 0 and colonne - 1 >= 0 and grid[ligne - 1][colonne - 1] in pieces_noires:
        liste_depl.append((ligne - 1, colonne - 1))
    if ligne - 1 >= 0 and colonne + 1 <= 7 and grid[ligne - 1][colonne + 1] in pieces_noires:
        liste_depl.append((ligne - 1, colonne + 1))
    return liste_depl


def depl_fou_noir_possible(grid, position):  # arguments: état du plateau (matrice), position (tuple),renvoie liste de tuples des cases possibles
    liste_depl = []
    ligne, colonne = position
    while ligne >= 0 and colonne >= 0:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_blanches:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_noires and (ligne, colonne) != position:
            break
        ligne -= 1
        colonne -= 1
    ligne, colonne = position
    while ligne <= 7 and colonne <= 7:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_blanches:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_noires and (ligne, colonne) != position:
            break
        ligne += 1
        colonne += 1
    ligne, colonne = position
    while ligne <= 7 and colonne >= 0:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_blanches:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_noires and (ligne, colonne) != position:
            break
        ligne += 1
        colonne -= 1
    ligne, colonne = position
    while ligne >= 0 and colonne <= 7:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_blanches:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_noires and (ligne, colonne) != position:
            break
        colonne += 1
        ligne -= 1
    return liste_depl


def depl_fou_blanc_possible(grid, position):  # arguments: état du plateau (matrice), position (tuple),renvoie liste de tuples des cases possibles
    liste_depl = []
    ligne, colonne = position
    while ligne >= 0 and colonne >= 0:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_noires:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_blanches and (ligne, colonne) != position:
            break
        ligne -= 1
        colonne -= 1
    ligne, colonne = position
    while ligne <= 7 and colonne <= 7:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_noires:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_blanches and (ligne, colonne) != position:
            break
        ligne += 1
        colonne += 1
    ligne, colonne = position
    while ligne <= 7 and colonne >= 0:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_noires:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_blanches and (ligne, colonne) != position:
            break
        ligne += 1
        colonne -= 1
    ligne, colonne = position
    while ligne >= 0 and colonne <= 7:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_noires:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_blanches and (ligne, colonne) != position:
            break
        colonne += 1
        ligne -= 1
    return liste_depl


def depl_tour_noire_possible(grid, position):  # arguments: état du plateau (matrice), position (tuple),renvoie liste de tuples des cases possibles
    liste_depl = []
    ligne, colonne = position
    while ligne >= 0 and colonne >= 0:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_blanches:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_noires and (ligne, colonne) != position:
            break
        ligne -= 1
    ligne, colonne = position
    while ligne <= 7 and colonne <= 7:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_blanches:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_noires and (ligne, colonne) != position:
            break
        ligne += 1
    ligne, colonne = position
    while ligne <= 7 and colonne >= 0:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_blanches:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_noires and (ligne, colonne) != position:
            break
        colonne -= 1
    ligne, colonne = position
    while ligne >= 0 and colonne <= 7:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_blanches:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_noires and (ligne, colonne) != position:
            break
        colonne += 1
    return liste_depl


def depl_tour_blanche_possible(grid, position):  # arguments: état du plateau (matrice), position (tuple),renvoie liste de tuples des cases possibles
    liste_depl = []
    ligne, colonne = position
    while ligne >= 0 and colonne >= 0:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_noires:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_blanches and (ligne, colonne) != position:
            break
        ligne -= 1
    ligne, colonne = position
    while ligne <= 7 and colonne <= 7:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_noires:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_blanches and (ligne, colonne) != position:
            break
        ligne += 1
    ligne, colonne = position
    while ligne <= 7 and colonne >= 0:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_noires:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_blanches and (ligne, colonne) != position:
            break
        colonne -= 1
    ligne, colonne = position
    while ligne >= 0 and colonne <= 7:
        if grid[ligne][colonne] == '  ':
            liste_depl.append((ligne, colonne))
        if grid[ligne][colonne] in pieces_noires:
            liste_depl.append((ligne, colonne))
            break
        if grid[ligne][colonne] in pieces_blanches and (ligne, colonne) != position:
            break
        colonne += 1
    return liste_depl


def depl_dame_blanche_possible(grid, position):  # arguments: état du plateau (matrice), position (tuple),renvoie liste de tuples des cases possibles
    liste1 = depl_fou_blanc_possible(grid, position)
    liste2 = depl_tour_blanche_possible(grid, position)
    dep = reunion(liste1, liste2)
    return dep


def depl_dame_noire_possible(grid, position):  # arguments: état du plateau (matrice), position (tuple),renvoie liste de tuples des cases possibles
    liste1 = depl_fou_noir_possible(grid, position)
    liste2 = depl_tour_noire_possible(grid, position)
    dep = reunion(liste1, liste2)
    return dep


def mouvements_possibles(piece, grid):
    couleur = piece['type']['couleur']
    type = piece['type']['type']
    position = piece['position']
    if type == 'pion':
        if couleur == 'blanc':
            return depl_pion_blanc_possible(grid, position)
        return depl_pion_noir_possible(grid, position)
    if type == 'fou':
        if couleur == 'blanc':
            return depl_fou_blanc_possible(grid, position)
        return depl_fou_noir_possible(grid, position)
    if type == 'cavalier':
        return depl_cavalier_possible(grid, position)
    if type == 'roi':
        x, y = position
        return depl_roi_possible(grid, x, y)
    if type == 'tour':
        if couleur == 'blanc':
            return depl_tour_blanche_possible(grid, position)
        return depl_tour_noire_possible(grid, position)
    if type == 'dame':
        if couleur == 'blanc':
            return depl_dame_blanche_possible(grid, position)
        return depl_dame_noire_possible(grid, position)


def ce_mouvement_est_possible(liste_pieces_noires, liste_pieces_blanches, xdebut, ydebut, xfin, yfin):  # prend en compte les situations d'échec
    grid = creer_matrice_pieces(liste_pieces_noires, liste_pieces_blanches)
    piece = trouver_piece((xdebut, ydebut), liste_pieces_noires, liste_pieces_blanches)
    if (xfin, yfin) in mouvements_possibles(piece, grid):
        piece['position'] = (xfin, yfin)
        couleur = piece['type']['couleur']
        test_grid = creer_matrice_pieces(liste_pieces_noires, liste_pieces_blanches)
        if not est_en_echec(liste_pieces_blanches, liste_pieces_noires, couleur, test_grid):
            piece['position'] = (xdebut, ydebut)
            return True
        piece['position'] = (xdebut, ydebut)     # on retrouve la grille d'origine
    return False


def afficher_liste_mouvements_possibles(coordonnees, grid, LN, LB):
    piece = trouver_piece(coordonnees, LN, LB)
    move_possible = mouvements_possibles(piece, grid)
    return move_possible


"""FIN PARTIE POSSIBLE"""




