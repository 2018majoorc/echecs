# définition des pièces, sous forme d'un dictionnaire contenant le type et la couleur


PN = dict()
PN['type'] = 'pion'
PN['couleur'] = 'noir'
PN['nom'] = 'PN'
PB = dict()
PB['type'] = 'pion'
PB['couleur'] = 'blanc'
PB['nom'] = 'PB'
CN = dict()
CN['type'] = 'cavalier'
CN['couleur'] = 'noir'
CN['nom'] = 'CN'
CB = dict()
CB['type'] = 'cavalier'
CB['couleur'] = 'blanc'
CB['nom'] = 'CB'
TN = dict()
TN['type'] = 'tour'
TN['couleur'] = 'noir'
TN['nom'] = 'TN'
TB = dict()
TB['type'] = 'tour'
TB['couleur'] = 'blanc'
TB['nom'] = 'TB'
FN = dict()
FN['type'] = 'fou'
FN['couleur'] = 'noir'
FN['nom'] = 'FN'
FB = dict()
FB['type'] = 'fou'
FB['couleur'] = 'blanc'
FB['nom'] = 'FB'
DN = dict()
DN['type'] = 'dame'
DN['couleur'] = 'noir'
DN['nom'] = 'DN'
DB = dict()
DB['type'] = 'dame'
DB['couleur'] = 'blanc'
DB['nom'] = 'DB'
RN = dict()
RN['type'] = 'roi'
RN['couleur'] = 'noir'
RN['nom'] = 'RN'
RB = dict()
RB['type'] = 'roi'
RB['couleur'] = 'blanc'
RB['nom'] = 'RB'


pieces_blanches = ['TB', 'PB', 'CB', 'FB', 'RB', 'DB']
pieces_noires = ['TN', 'PN', 'CN', 'FN', 'RN', 'DN']


def liste_pieces_noires_init():
    liste = [{'type': TN, 'position': (0, 0)}, {'type': CN, 'position': (0, 1)},
           {'type': FN, 'position': (0, 2)}, {'type': DN, 'position': (0, 3)},
           {'type': RN, 'position': (0, 4)}, {'type': FN, 'position': (0, 5)},
           {'type': CN, 'position': (0, 6)}, {'type': TN, 'position': (0, 7)},
           {'type': PN, 'position': (1, 0)}, {'type': PN, 'position': (1, 1)},
           {'type': PN, 'position': (1, 2)}, {'type': PN, 'position': (1, 3)},
           {'type': PN, 'position': (1, 4)}, {'type': PN, 'position': (1, 5)},
           {'type': PN, 'position': (1, 6)}, {'type': PN, 'position': (1, 7)}]
    return liste


def liste_pieces_blanches_init():
    liste = [{'type': TB, 'position': (7, 0)}, {'type': CB, 'position': (7, 1)},
           {'type': FB, 'position': (7, 2)}, {'type': DB, 'position': (7, 3)},
           {'type': RB, 'position': (7, 4)}, {'type': FB, 'position': (7, 5)},
           {'type': CB, 'position': (7, 6)}, {'type': TB, 'position': (7, 7)},
           {'type': PB, 'position': (6, 0)}, {'type': PB, 'position': (6, 1)},
           {'type': PB, 'position': (6, 2)}, {'type': PB, 'position': (6, 3)},
           {'type': PB, 'position': (6, 4)}, {'type': PB, 'position': (6, 5)},
           {'type': PB, 'position': (6, 6)}, {'type': PB, 'position': (6, 7)}]
    return liste


def creer_matrice_pieces(liste_pieces_noires, liste_pieces_blanches):
    #  Prend en arguments deux listes de dictionnaires, et fabrique la matrice associée
        matrice_pieces = [[], [], [], [], [], [], [], []]
        LN = liste_pieces_noires
        LB = liste_pieces_blanches
        for indice in range(8):
            for iteration in range(8):
                matrice_pieces[indice].append('  ')
        for piece in LN:
            ligne, colonne = piece['position']
            matrice_pieces[ligne][colonne] = piece['type']['nom']
        for piece in LB:
            ligne, colonne = piece['position']
            matrice_pieces[ligne][colonne] = piece['type']['nom']
        return matrice_pieces



