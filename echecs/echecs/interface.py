import tkinter as tk
#from PIL import Image, ImageTk
from echecs.initialisation import liste_pieces_blanches_init
from echecs.initialisation import liste_pieces_noires_init
from echecs.initialisation import creer_matrice_pieces
from echecs.game import est_echec_et_mat2
from echecs.possible import est_en_echec
from echecs.possible import trouver_piece
from echecs.possible import afficher_liste_mouvements_possibles
from echecs.game import mouvement


global LN,LB,M, compteur2, compteur_couleur, liste_coordonnees
LN = liste_pieces_noires_init()
LB = liste_pieces_blanches_init()
M = creer_matrice_pieces(LN, LB)
compteur2=0
compteur_couleur=0
liste_coordonnees = []


def modifier_affichage(x): #il faut encore remplir cette fonction
    global compteur2, compteur_couleur #sert à compter 2 clics sur le plateau avant d'effectuer la mise à jour
    global liste_coordonnees
    global plateau

    global LN, LB, M
    #if compteur2==2:
        #xdebut, ydebut = liste_coordonnees[0]
        #xfin, yfin = liste_coordonnees[1]
        #mouvement(xdebut, ydebut, xfin, yfin, LB, LN)
        #liste_coordonnees = []
        #compteur2=0 #on remet le compteur à 0 pour compter les 2 clics du tour suivant
        #compteur_couleur += 1
    if compteur2==0:
        if compteur_couleur % 2 == 0:
                piece = trouver_piece(x, LN, LB)
                if piece['type']['couleur'] == 'blanc':
                    liste_coordonnees.append(x) #récuperer les coordonnées de départ (à remplir)
                    compteur2 += 1
        else:
                piece = trouver_piece(x, LN, LB)
                if piece['type']['couleur'] == 'noir':
                    liste_coordonnees.append(x) #récuperer les coordonnées de départ (à remplir)
                    compteur2 += 1

    else:
        xdebut, ydebut = liste_coordonnees[0]
        liste_mouvements_possibles = afficher_liste_mouvements_possibles((xdebut, ydebut), M, LN, LB)
        xfin, yfin = x
        if x in liste_mouvements_possibles:
            liste_coordonnees.append(x) #récupérer les coordonnées d'arrivée (à remplir)
            LB, LN = mouvement(LN, LB, liste_coordonnees[0][0], liste_coordonnees[0][1], liste_coordonnees[1][0], liste_coordonnees[1][1])
            M = creer_matrice_pieces(LB, LN)
            compteur2 = 0
            compteur_couleur += 1
            liste_coordonnees = []
            jouer()









root=tk.Tk() #fenêtre du menu
root.title('EcheCS')
root.configure(background='black')
message=tk.Label(root, text='Bienvenue dans EcheCS !\n', bg='black', fg='white', font='None 25 bold').grid(row=0,column=0)
#image=Image.open('220px-ChessSet.jpg')
#photo=ImageTk.PhotoImage(image)
#canvas=tk.Canvas(root,width = image.size[0], height= image.size[1])
#canvas.create_image(0,0,anchor = tk.NW, image=photo)
#canvas.grid(row=1,column=0)
def jouer(): #c'est là dedans qu'il faudra mettre la fonction générale de jeu
    for x in root.winfo_children():
                x.destroy()
    root.wm_state('iconic')
    global compteur_couleur
    plateau=tk.Toplevel(root) #fenêtre du plateau
    plateau.geometry('+0+0')

    liste_cases=[]
    liste_boutons=[]
    global liste_coordonnees
    for i in range(0,8):
       for j in range(0,8):
           if (i+j)%2 == 0:
                case=tk.Frame(plateau,bg='white',height=50,width=50).grid(row=i, column=j)
                liste_cases.append(case)
           else:
               case=tk.Frame(plateau,bg='black',height=50,width=50).grid(row=i, column=j)
               liste_cases.append(case)
    for i in range(len(M)): #grid définie ligne 3
        for j in range(len(M)):
            if (i+j)%2 == 0:
                bouton_selection=tk.Button(plateau, liste_cases[8*i+j],bg='white', fg='black', text=M[i][j], font='None 18 bold',height=2,width=4,relief='flat', activebackground='white', command=lambda i_courant=i, j_courant=j : modifier_affichage( (i_courant,j_courant) )).grid(row=i, column=j)
                liste_boutons.append(bouton_selection)
            else:
                bouton_selection=tk.Button(plateau, liste_cases[8*i+j],bg='black', fg='white', text=M[i][j], font='None 18 bold', height=2, width=4, relief='flat', activebackground='black', command=lambda i_courant=i, j_courant=j :modifier_affichage( (i_courant,j_courant) )).grid(row=i, column=j)
                liste_boutons.append(bouton_selection)
    if compteur_couleur % 2 == 1:
        if est_en_echec(LN, LB, 'blanc', M):
            if est_echec_et_mat2(LB, LN, 'noir', M):
                echec_mat = tk.Toplevel(root)
                echec_mat.configure(background='black')
                echec_mat.geometry('+560+0')
                tk.Label(echec_mat, text="Echec et mat du roi noir\nLe joueur 1 remporte la partie", bg='black', fg='white', font='None 20 bold').grid(row=0,column=0)
                bouton_exit=tk.Button(echec_mat,text='Quitter',font='None 22 bold', bg='black', fg='white', bd=10, command= quit).grid(row=1,column=0)
                echec_mat.mainloop()
            tk.Label(plateau, text="Roi noir en échec", bg='#B97D00', font='None 20 bold', fg='white').grid(row=8, column=0, columnspan=6)
            plateau.configure(background='#B97D00')
    elif compteur_couleur % 2 == 0:
        if est_en_echec(LN, LB, 'noir', M):
            if est_echec_et_mat2(LB, LN, 'blanc', M):
                echec_mat = tk.Toplevel(root)
                tk.Label(echec_mat, text="Echec et mat du roi blanc\nLe joueur 1 remporte la partie", bg='white', fg='black', font='None 20 bold').pack()
                bouton_exit=tk.Button(echec_mat,text='Quitter',font='None 22 bold', bg='white', bd=10, command= quit).grid(row=1,column=0)
                plateau.destroy()
                echec_mat.mainloop()
            tk.Label(plateau, text="Roi\nblanc\nen\néchec", bg='#B97D00', font='None 20 bold', fg='red').grid(row=8, column=3, columnspan=2)
            plateau.configure(background='#B97D00')
    bouton_exit=tk.Button(plateau,text='Quitter', bd=10, bg='black', fg='white', font='None 22 bold', command= quit).grid(row=8,column=6, columnspan=2, sticky='E')
    plateau.mainloop()
bouton_jouer=tk.Button(root,text='Jouer', bg='grey', fg='white', font='None 20 bold', bd=5, command=jouer).grid(row=2,column=0)
root.mainloop()


