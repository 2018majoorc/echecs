Groupe 12, Coding Weeks semaine 2

Equipe : Colin Majoor, Hippolyte Sibileau, Clement Cosserat, Marien Chenaud

Nom du projet : EcheCS

Objectif général : Programmation d'un jeu d'échecs sur Python pour faire s'affronter deux joueurs, 
avec possibilité d'insérer des questions mathématiques pour obtenir des avantages dans le jeu

Décomposition du projet en objectifs :

Objectif 1 : Programmation des déplacements de base du jeu (Pas d'interface graphique)
    * Sprint 0 : Création du projet sur Gitlab
    * Sprint 1 : Mise en place des données du jeu
       Fonctionnalité 1 : choix des types pertinents pour représenter les pièces de l'échiquier
       Fonctionnalité 2 : représentation de la grille et des pièces
    * Sprint 2 : Gestion des déplacements des pièces
       Fonctionnalité 3 : Déterminaion des mouvements possibles pour une pièce donnée 
       Fonctionnalité 4 : reconnaissance des situations dans lesquelles un joueur est en echec

Objectif 2 : Jeu entre deux joueurs
    * Sprint 3 : mise en place des tests des fonctions de l'objectif 1
    * Sprint 4 : Reconnaissance de la fin du jeu
       Fonctionnalité 5 : reconnaissance des situations d'"échec et mat"
    * Sprint 5 : Jeu
       Fonctionnalité 6 : Faire jouer deux joueurs entre eux
       
FIN DU MVP

Objectif 3 : Jeu d'échecs complet, avec une interface graphique
    * Sprint 6 : Création de l'interface graphique pour la grille de jeu
       Fonctionnalité 7 : Affichage de l'échiquier
       Fonctionnalité 8 : Interface graphique pour récupérer les commandes entrées par les joueurs

Nous nous sommes arrêtés ici.

Pour aller plus loin : 
 - Programmer les déplacements + complexes (prise en passant et roque),
 - Améliorer la jouabilité en affichant les mouvements possibles sur l'échiquier pour une pièce donnée
 - Afficher les pièces mangées en cours de partie
 - Insérer des questions mathématiques auxquelles doivent répondre les joueurs sous peine de pénalité 
    (par exemple, si un joueur répond mal il doit passer son tour)
 - Programmer le jeu avec l'ordinateur, et créer une IA apprenant à jouer...

MVP : Objectifs 1 et 2

