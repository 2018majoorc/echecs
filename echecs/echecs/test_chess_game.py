# Test des fonctions liées au jeu (échecs, cases menacées...)

from echecs.initialisation import *
from echecs.possible import cases_menacees
from echecs.possible import est_en_echec
from echecs.possible import est_bloque
from echecs.possible import est_echec_et_mat
from echecs.possible import depl_pion_noir_possible
from echecs.possible import depl_fou_noir_possible
from echecs.possible import depl_cavalier_possible
from echecs.possible import depl_tour_noire_possible
from echecs.game import est_echec_et_mat2
from echecs.possible import ce_mouvement_est_possible

liste_blancs_mat = [{'type' : RB, 'position' : (0,0)}, {'type' : PB, 'position' : (6,6)}]   # situation où les blancs sont en echec et mat
liste_noirs_mat = [{'type' : RN, 'position' : (7,7)}, {'type' : DN, 'position' : (1,1)}, {'type' : TN, 'position' : (1,0)}]
test_grid = creer_matrice_pieces(liste_blancs_mat, liste_noirs_mat)
liste_menace = [{'type' : PB, 'position': (4,4)}]       # liste test pour les cases menacées par un pion
grid_menace = creer_matrice_pieces(liste_menace, [])


def test_cases_menacees():
    assert set(cases_menacees(liste_blancs_mat, test_grid)) == {(0, 1), (1, 0), (1, 1),(5,7),(5,5)}
    assert set(cases_menacees(liste_menace, grid_menace)) == {(3,3),(3,5)}

def test_echec():
    assert est_en_echec(liste_blancs_mat, liste_noirs_mat, 'blanc', test_grid)
    assert not est_en_echec(liste_blancs_mat, liste_noirs_mat, 'noir', test_grid)

def test_est_bloque():
    assert est_bloque(liste_blancs_mat, liste_noirs_mat, 'blanc', test_grid)
    assert not est_bloque(liste_blancs_mat, liste_noirs_mat, 'noir', test_grid)


def test_echec_mat():
    assert not est_echec_et_mat(liste_blancs_mat, liste_noirs_mat,'noir', test_grid)
    assert est_echec_et_mat(liste_blancs_mat, liste_noirs_mat, 'blanc', test_grid)

def test_echec_et_mat2():
    assert not est_echec_et_mat2(liste_blancs_mat, liste_noirs_mat,'noir', test_grid)
    assert est_echec_et_mat2(liste_blancs_mat, liste_noirs_mat, 'blanc', test_grid)

# Test des fonctions de déplacement de pièces


pieces_blanches = ['TB', 'PB', 'CB', 'FB', 'RB', 'DB']
pieces_noires = ['TN', 'PN', 'CN', 'FN', 'RN', 'DN']
def test_depl_possible():
    #pions
    assert depl_pion_noir_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','PN','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,1))==[(3,1),(2,1)]
    assert depl_pion_noir_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','PN','  ','  ','  ','  ','  ','  '],['  ','TB','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,1))==[]
    assert depl_pion_noir_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['PN','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(2,0))==[(3,0)]
    assert depl_pion_noir_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','PN','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['PN',' ',' ',' ',' ',' ',' ',' ']],(7,0))==[]
    assert depl_pion_noir_possible([['  ','PN','  ','  ','  ','  ','  ','  '],['TB','  ','DB','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(0,1))==[(1,1),(1,0),(1,2)]
    #tour
    assert depl_tour_noire_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','TN','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,1))==[(0,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(1,0),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7)]
    assert depl_tour_noire_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','TN','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','CB','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,1))==[(0,1),(2,1),(3,1),(1,0),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7)]
    assert depl_tour_noire_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','TN','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','CN','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,1))==[(0,1),(2,1),(1,0),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7)]
    #fou
    assert depl_fou_noir_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','FN','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,1))== [(0,0),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(2,0),(0,2)]
    assert depl_fou_noir_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','FN','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','CB','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,1)) == [(0,0),(2,2),(3,3),(2,0),(0,2)]
    assert depl_fou_noir_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','FN','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','CN','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,1)) == [(0,0),(2,2),(2,0),(0,2)]
    #cavalier
    assert depl_cavalier_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','CN','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,6)) == [(0,4),(3,5),(2,4),(3,7)]
    assert depl_cavalier_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','CN','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','FB','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,6)) == [(0,4),(3,5),(2,4),(3,7)]
    assert depl_cavalier_possible([['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','CN','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','FN','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  '],['  ','  ','  ','  ','  ','  ','  ','  ']],(1,6)) == [(0,4),(2,4),(3,7)]

liste_blancs_mouvement_possible = [{'type' : RB, 'position' : (7,5)}, {'type' : CB, 'position' : (6,5)}]
liste_noirs_mouvement_possible = [{'type' : TN, 'position' : (0,5)}, {'type' : RN, 'position' : (0,0)}]
grid_possible = creer_matrice_pieces(liste_noirs_mouvement_possible, liste_blancs_mouvement_possible)

def test_mouvement_possible():
    assert not ce_mouvement_est_possible(liste_noirs_mouvement_possible, liste_blancs_mouvement_possible, 6,5,5,5)
    assert not ce_mouvement_est_possible(liste_noirs_mouvement_possible, liste_blancs_mouvement_possible, 6,5,4,4)
    assert ce_mouvement_est_possible(liste_noirs_mouvement_possible, liste_blancs_mouvement_possible, 7,5,7,6)



